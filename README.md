<h1>Kotlin CRUD</h1>

A simple Kotlin CRUD application, copied from a tutorial from javalin.io. The goal of this project was simply to get a bit more familiar with Kotlin.