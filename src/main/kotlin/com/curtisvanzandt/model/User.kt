package com.curtisvanzandt.model

data class User(val name: String, val email: String, val id: Int)